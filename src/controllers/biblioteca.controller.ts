import { Request, Response, NextFunction } from 'express';
import { Biblioteca } from '../negocio/biblioteca';

export async function getLivro(req: Request, res: Response, next: NextFunction) {
    try {
        const id = req.params.id;
        const livro = await Biblioteca.buscarLivroPorId(id);
        if (livro !== null) {
            res.json(livro);
        } else {
            res.sendStatus(404);
        }
    } catch (error) {
        next(error);
    }
}