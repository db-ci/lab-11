import { Router } from 'express';
import * as BibliotecaController from '../controllers/biblioteca.controller';

export const router = Router();
export const path = '/livros';
router.get(`${path}/:id`, BibliotecaController.getLivro);