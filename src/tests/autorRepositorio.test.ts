import mongoose from 'mongoose';
import * as dbhandler from './dbhandler';
import { AutorRepositorio } from '../persistencia/autorRepositorio';
import { Autor } from '../entidades/autor';
import { AutorModel } from '../persistencia/autorModel';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 600000;

/**
 * Conecta com o banco de dados antes de executar qualquer teste.
 */
beforeAll(async () => {
    await dbhandler.connect();
});

/**
 * Limpa os dados após cada teste.
 */
afterEach(async () => {
    await dbhandler.clearDatabase();
});

/**
 * Remove e fecha a conexão com o banco de dados após os teste.
 */
afterAll(async () => {
    await dbhandler.closeDatabase();
});

describe('AutorRepositorio', () => {
    const mockAutor = {
        primeiro_nome: "John",
        ultimo_nome: "Doe"
    };
    const mockAutorPrimeiroNomeErroMin = {
        primeiro_nome: "",
        ultimo_nome: "Doe"
    };
    const mockAutorUltimoNomeErroMin = {
        primeiro_nome: "John",
        ultimo_nome: ""
    };
    describe('criar() integração', () => {
        test('deve inserir um autor sem erros', async () => {
            expect(async () => {
                await AutorRepositorio.criar(mockAutor);
            }).not.toThrow();
        });
        test('deve inserir um autor e obter objeto inserido no bd', async () => {
            const resultAutor = await AutorRepositorio.criar(mockAutor);
            expect(resultAutor.primeiro_nome).toEqual(mockAutor.primeiro_nome);
            expect(resultAutor.ultimo_nome).toEqual(mockAutor.ultimo_nome);
        });
        test('requer autor com primeiro_nome e ultimo_nome válidos', async () => {
            await expect(AutorRepositorio.criar(mockAutorPrimeiroNomeErroMin))
                  .rejects
                  .toThrow(mongoose.Error.ValidationError);
            await expect(AutorRepositorio.criar(mockAutorUltimoNomeErroMin))
                  .rejects
                  .toThrow(mongoose.Error.ValidationError);
        });
    });
    describe('buscar() integração', () => {
        test('deve retornar uma coleção uma vazia', async () => {
            const resultAutores = await AutorRepositorio.buscar();
            expect(resultAutores).toBeDefined();
            expect(resultAutores).toHaveLength(0);
        });
        test('deve retornar uma coleção com dois autores', async() => {
            await seedDatabase();
            const resultAutores = await AutorRepositorio.buscar();
            expect(resultAutores).toBeDefined();
            expect(resultAutores).toHaveLength(2);
            expect(resultAutores[0].primeiro_nome).toEqual('John');
        });
    });
});

async function seedDatabase() {
    await AutorModel.create({
        primeiro_nome: "John",
        ultimo_nome: "Doe"
    });
    await AutorModel.create({
        primeiro_nome: "Mary",
        ultimo_nome: "Doe"
    });
}